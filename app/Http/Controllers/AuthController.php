<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\OtpVerification;
use App\Models\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Response;
use Twilio\Rest\Client;
use Twilio\TwiML\MessagingResponse;
use function Composer\Autoload\includeFile;

class AuthController extends Controller
{
  /**
  * Create user
  *
  * @param  [string] name
  * @param  [string] email
  * @param  [string] password
  * @param  [string] password_confirmation
  * @return [string] message
  */
  public function register(Request $request)
  {
      try{
      $validator = Validator::make($request->all(), [
          'name' => 'required|string',
          'password' => 'required',
//          'address' => 'required',
//          'pincode' => 'required',
          'phone' => 'required',
      ]);

      if ($validator->fails()) {
          $response = ['flag' => false, 'message' => $validator->messages()->all()];
      } else {
          $mobileexit = User::where('mobile_no', $request->phone)->where('mobile_verify',1)->first();

          if (!$mobileexit) {
                  $requestData = $request->all();
                  $requestData['password'] = md5($request->password);
                  $requestData['mobile_no'] = $requestData['phone'];
                  unset($requestData['phone']);
                  $user = User::updateOrCreate(['mobile_no' => $requestData['mobile_no']],$requestData);
                  if ($user) {
                      $sid = env('TWILIO_ACCOUNT_SID');
                      $token = env('TWILIO_AUTH_TOKEN');
                      $client = new Client($sid, $token);
                      $response = new MessagingResponse();
                      $message = $response->message('');
                      $message->body('Hello World!');
                      $response->redirect('https://demo.twilio.com/welcome/sms/');

                      if (env('APP_ENV') == 'local')
                      {
                          $otp= env('DEMO_OTP');
                      }
                      else {
                          $otp = rand(111111, 999999);
                      }
                      $mobile = '+91' . $requestData['mobile_no'];
                      $message = 'Your mobile verification code is: ' . $otp . '.';
                      $data = array('otp' => $otp, 'type_value' => $requestData['mobile_no'], 'type' => 'mobile');
                      OtpVerification::updateOrCreate(['type_value' => $user->mobile], $data);

//                      $message = $client->messages
//                          ->create("whatsapp:".$mobile, // to
//                              [
//                                  "from" =>"whatsapp:".env("TWILIO_WhatsApp"),
//                                  "body" => "Hello there!"
//                              ]
//                          );
                      if (App::environment('production'))
                      {
                          $client->messages->create($mobile, array('from' => env('TWILIO_APP_SID'), 'body' => $message));
                      }
                      $response = ['flag' => true, 'message' => 'User add successfully.', 'user' => $user];
                  } else {
                      $response = ['flag' => false, 'message' => 'Something wrong.'];
                  }

          } else {

              $response = ['flag' => false, 'message' => 'User exist with this mobile number.'];
          }

      }
//      dd($response);
      return Response::json($response, 200);
  }
  catch (Exception $e) {

      return Response::json(['flag' => false, 'error' => $e->getMessage()], 400);

  }

  }

    public function login(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'phone' => 'required',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                $response = ['flag' => false, 'message' => $validator->messages()->all()];
            } else {
                $mobileexit = User::where('mobile_no', $request->phone)->where('password',md5($request->password))->first();
                if ($mobileexit) {
                    $activeUser = $mobileexit->status;
//                    $mobile_verify = $mobileexit->mobile_verify;
                    if ($activeUser == 1) {
//                        if ($mobile_verify == 1) {
                            $response = ['flag' => true, 'message' => 'User login successfully.', 'user' => $mobileexit];
//                        } else {
//                            $response = ['flag' => false, 'message' => 'User Mobile number not verified yet.'];
//                        }
                    }else {
                        $response = ['flag' => false, 'message' => 'User not active.'];
                    }
                }
                else {

                    $response = ['flag' => false, 'message' => 'User does not exist with this mobile number.'];
                }

            }
//      dd($response);
            return Response::json($response, 200);
        }
        catch (Exception $e) {

            return Response::json(['flag' => false, 'error' => $e->getMessage()], 400);

        }

    }

    public function otpVerification(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'phone' => 'required',
                'otp' => 'required',
            ]);

            if ($validator->fails()) {
                $response = ['flag' => false, 'message' => $validator->messages()->all()];
            } else {
                $mobileexit = User::where('mobile_no', $request->phone)->first();
                if ($mobileexit) {

                  $otpMatch =  OtpVerification::where('type_value',$request->phone)->where('otp',$request->otp)->first();
                  if ($otpMatch) {
                      User::where('mobile_no',$request->phone)->update(['mobile_verify'=>1,'status' =>1]);
                      $response = ['flag' => true, 'message' => 'User register successfully.','user' =>$mobileexit];
                  }
                  else{
                      $response = ['flag' => false, 'message' => 'Otp doest not match.'];
                  }
                }

                else {

                    $response = ['flag' => false, 'message' => 'User does not exist with this mobile number.'];
                }

            }
//      dd($response);
            return Response::json($response, 200);
        }
        catch (Exception $e) {

            return Response::json(['flag' => false, 'error' => $e->getMessage()], 400);

        }

    }

    public function updateUser(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                $response = ['flag' => false, 'message' => $validator->messages()->all()];
            } else {
                $mobileexit = User::find($request->id);

                if ($mobileexit) {
                        $requestData = $request->all();
                        if (isset($request->phone)) {
                            $requestData['password'] = md5($request->phone);
                        }
                        if (isset($requestData['phone'])) {
                            $requestData['mobile_no'] = $requestData['phone'];
                            unset($requestData['phone']);
                        }

                        $user = User::updateOrCreate(['id' => $request->id],$requestData);
                    $response = ['flag' => true, 'message' => 'User updated.'];

                    }
                else {

                    $response = ['flag' => false, 'message' => 'User does not exit.'];
                }

            }
//      dd($response);
            return Response::json($response, 200);
        }
        catch (Exception $e) {

            return Response::json(['flag' => false, 'error' => $e->getMessage()], 400);

        }
    }


    public function resendOtp(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'phone' => 'required',
            ]);

            if ($validator->fails()) {
                $response = ['flag' => false, 'message' => $validator->messages()->all()];
            } else {
                $mobileexit = User::where('mobile_no', $request->phone)->first();
                if ($mobileexit) {
                    if (env('APP_ENV') == 'local')
                    {
                        $otp= env('DEMO_OTP');
                    }
                    else {
                        $otp = rand(111111, 999999);
                    }
//                    dd(env('APP_ENV'));
                      $sid = env('TWILIO_ACCOUNT_SID');
                      $token = env('TWILIO_AUTH_TOKEN');
                      $client = new Client($sid, $token);
                    $mobile = '+91' . $request->phone;
                    $message = 'Your mobile verification code is: ' . $otp . '.';
                    $data = array('otp' => $otp, 'type_value' => $request->phone, 'type' => 'mobile');
                    OtpVerification::updateOrCreate(['type_value' => $request->phone], $data);
                       if (App::environment('production'))
                      {
                    $message =  $client->messages->create($mobile, array('from' => env('TWILIO_APP_SID'), 'body' => $message));
                      }
//                    dd($message->sid);
                    $response = ['flag' => true, 'message' => 'Otp resend successfully.'];
                }

                else {

                    $response = ['flag' => false, 'message' => 'User does not exist with this mobile number.'];
                }

            }
//      dd($response);
            return Response::json($response, 200);
        }
        catch (Exception $e) {

            return Response::json(['flag' => false, 'error' => $e->getMessage()], 400);

        }
    }

    public function forgotPassword(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'phone' => 'required',
            ]);

            if ($validator->fails()) {
                $response = ['flag' => false, 'message' => $validator->messages()->all()];
            } else {
                $mobileexit = User::where('mobile_no', $request->phone)->first();
//                dd($mobileexit);
                if ($mobileexit) {

//                    dd(env('APP_ENV'));
                    $sid = env('TWILIO_ACCOUNT_SID');
                    $token = env('TWILIO_AUTH_TOKEN');
                    $client = new Client($sid, $token);
                    $mobile = '+91' . $request->phone;
                    $message = 'Your dicegame password is : ' . $mobileexit->password . '.';
                   if (App::environment('production'))
                    {
                        $message =  $client->messages->create($mobile, array('from' => env('TWILIO_APP_SID'), 'body' => $message));
                    }
//                    dd($message->sid);
                    $response = ['flag' => true, 'message' => 'We have send sms for your password.'];
                }

                else {

                    $response = ['flag' => false, 'message' => 'User does not exist with this mobile number.'];
                }

            }
//      dd($response);
            return Response::json($response, 200);
        }
        catch (Exception $e) {

            return Response::json(['flag' => false, 'error' => $e->getMessage()], 400);

        }
    }

}

