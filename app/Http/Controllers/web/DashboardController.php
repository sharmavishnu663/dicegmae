<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\OtpVerification;
use App\Models\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Response;
use Twilio\Rest\Client;

class DashboardController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard');
    }


    public function userList(Request $request)
    {
        $users = User::all();
        return view('admin.user.index',compact('users'));
    }

    public function employeeChangePassword(Request $request)
    {
        $employee = Employee::find($request->id);
        if ($employee)
        {
            $employee->password = md5($request->password);
            $employee->save();
            return Redirect::route('admin.employee.list')->with('success', 'Employee password change successfully');
        }else
        {
            return Redirect::route('admin.employee.list')->with('error', 'Employee not found!');
        }
    }

    public function adminManagement(Request $request)
    {
        $admin = Admin::all();
        return view('admin.management.index',compact('admin'));

    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');

    }
}

