<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\OtpVerification;
use App\Models\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Response;
use Twilio\Rest\Client;

class AuthController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function doLogin(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required');

        $validator = Validator::make($request->all() , $rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)
                ->withInput($request->except('password'));
        }
        else
        {$userdata = array(
                'email' => $request->email,
                'password' => $request->password);
            $password = $request->password;
            $adminDetails = Admin::where('email',$request->email)->first();
            if($adminDetails && Hash::check($password, $adminDetails->password))
            {
//                dd(Auth::attempt($userdata));
                if (Auth::attempt($userdata))
                {
//                    dd('done');
                return redirect()->route('dashboard');
                }
                else
                {
                    return Redirect::back()->withInput($request->except('password'))
                        ->withErrors(['Your login not match!']);
                }
            }
            else
            {
                return Redirect::back()->withInput($request->except('password'))
                    ->withErrors(['Something went wrong!']);
            }
        }
    }

}

