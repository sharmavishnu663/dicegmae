<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\OtpVerification;
use App\Models\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Response;
use Twilio\Rest\Client;

class EmployeeController extends Controller
{
    public function index()
    {
        return view('admin.employee.dashboard.login');
    }

    public function login(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required');

        $validator = Validator::make($request->all() , $rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)
                ->withInput($request->except('password'));
        }
        else
        {$userdata = array(
            'email' => $request->email,
            'password' => $request->password);
            $password = md5($request->password);

            $employeeDetails = Employee::where('email',$request->email)->where('password',$password)->first();
//            dd($employeeDetails);
            if($employeeDetails)
            {
                session(['employeeDetails' => $employeeDetails]);
                    return redirect()->route('employee.dashboard');
                }
                else
                {
                    return Redirect::back()->withInput($request->except('password'))
                        ->withErrors(['Your login not match!']);
                }

        }
    }

    public function dashboard(Request $request)
    {
        return view('admin.employee.dashboard.dashboard');
    }

    public function userList(Request $request)
    {
        $employee = session('employeeDetails');
        $users = User::with('employee')->where('emp_id',$employee->id)->get();
//        dd($users);
        return view('admin.employee.dashboard.user.index',compact('users'));
    }

    public function userAdd(Request $request)
    {

    }


}

