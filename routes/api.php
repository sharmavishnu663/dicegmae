<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::group([
//    'prefix' => 'auth'
//], function () {
//    Route::group([
//        'middleware' => 'auth:api'
//    ], function() {
        Route::post('register', 'App\Http\Controllers\AuthController@register')->name('register');
        Route::post('login', 'App\Http\Controllers\AuthController@login')->name('login');
        Route::post('otp-verification', 'App\Http\Controllers\AuthController@otpVerification')->name('otp.verification');
        Route::post('resend-otp', 'App\Http\Controllers\AuthController@resendOtp')->name('resernd.otp');
        Route::post('forgot-password', 'App\Http\Controllers\AuthController@forgotPassword')->name('forgot.password');
        Route::post('update-user', 'App\Http\Controllers\AuthController@updateUser')->name('update.user');

//    });
//});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
