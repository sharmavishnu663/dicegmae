<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', '\App\Http\Controllers\web\AuthController@login')->name('login');
Route::post('admin-login', '\App\Http\Controllers\web\AuthController@doLogin')->name('admin.login');
Route::get('admin-dashboard', '\App\Http\Controllers\web\DashboardController@dashboard')->name('dashboard');
Route::get('admin-employee-list', '\App\Http\Controllers\web\DashboardController@employeeList')->name('admin.employee.list');
Route::get('admin-users-list', '\App\Http\Controllers\web\DashboardController@userList')->name('admin.user.list');
Route::post('add-employee-details', '\App\Http\Controllers\web\DashboardController@addEmployee')->name('add.employee.details');
Route::post('edit-employee-details', '\App\Http\Controllers\web\DashboardController@editEmployee')->name('edit.employee.details');
Route::get('delete-employee/{id}', '\App\Http\Controllers\web\DashboardController@deleteEmployee')->name('delete.employee');
Route::get('admin/management', '\App\Http\Controllers\web\DashboardController@adminManagement')->name('admin.management');

Route::get('employee', '\App\Http\Controllers\web\EmployeeController@index')->name('employee.login');
Route::post('employee-login', '\App\Http\Controllers\web\EmployeeController@login')->name('employee.attempt');
Route::get('employee-dashboard', '\App\Http\Controllers\web\EmployeeController@dashboard')->name('employee.dashboard');
Route::get('employee-users-list', '\App\Http\Controllers\web\EmployeeController@userList')->name('employee.user.list');
Route::get('employee-add-user', '\App\Http\Controllers\web\EmployeeController@userAdd')->name('employee.add.user');
Route::post('employee-change-password', '\App\Http\Controllers\web\DashboardController@employeeChangePassword')->name('change.employee.password');
Route::get('logout', '\App\Http\Controllers\web\DashboardController@logout')->name('logout');
