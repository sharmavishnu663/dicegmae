<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
{{--                @if(Auth::user()->profile_image !='')--}}
{{--                    <img src="{{ Auth::user()->profile_image }}" class="img-circle elevation-2" alt="User Image">--}}
{{--                @else--}}
                <img src="{{ asset('admin/assets/img/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
{{--                    @endif--}}
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ session('employeeDetails')->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="{{route('employee.dashboard')}}" class="nav-link {{  Request::path() == 'employee-dashboard'? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('employee.user.list')}}" class="nav-link {{  Request::path() == 'employee-users-list'? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            User List
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
