@extends('admin.layout.app')
<!-- Content Wrapper. Contains page content -->

@section('content')
    <style>
        .save-content{
            margin-top: 8px;
            float: right;
        }
    </style>
<div class="content-wrapper" style="min-height: 1244.06px;">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <div class="card-header">
                        <h3 class="card-title">
                            Employee List
                        </h3>
                        <!-- tools box -->
                        <div class="card-tools">
                            <button  class="btn btn-primary"  data-toggle="modal" data-target="#officeAddModal" style="float: right">Add Employee</button>

                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body pad">
                        <div class="tab-content">

                            <!-- Tab panel -->
                            <div id="tab-pane1" class="tab-pane active">

                                <!-- Card Body -->
                                <div class="card-body">

                                    <!-- Card Title-->
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile Number</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($emploies as $employee)
                                            <tr>
                                                <td>{{ $employee->name }}</td>
                                                <td>{{ $employee->email }}</td>
                                                <td>{{ $employee->mobile_no }}</td>
                                                <td>{{ $employee->status == 0 ? 'Deactive':'Active' }}</td>
                                                <td>
                                                    <a class="js-edit-logo" data-toggle="modal" data-target="#officeEditModal" data-id="{{ $employee->id }}" data-name="{{ $employee->name }}"  data-email="{{ $employee->email }}" data-mobile="{{ $employee->mobile_no }}" data-status="{{ $employee->status }}"  style="cursor:pointer" title="edit state"><i class="fa fa-edit"></i></a>
                                                    <a class="delete-material" href="{{ route('delete.employee',$employee->id) }}"  data-id="{{ @$employee->id }}"  title="delete logo" onClick="return  confirm('Are you sure you want to delete ?')"><i class="fa fa-trash-alt"></i></a>
                                                    <a class="js-change-password" href="javascript:void(flase);" data-toggle="modal" data-target="#changepassword"  data-id="{{ @$employee->id }}" >Change Password</a>
                                                </td>
{{--                                                {{ route('delete.office',@$employee->id) }}--}}
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>

        <div id="officeAddModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Employee Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <form action="{{ route('add.employee.details') }}" method="post" enctype="multipart/form-data">

                        @csrf
                        <input type="hidden" name="id" id="officeid">
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="email-1">Name</label>
                                <input type="text" id="name" class="form-control" name="name" required>
                            </div>

                            <div class="form-group">
                                <label for="email-1">Email</label>
                                <input type="text" id="email" class="form-control" name="email" required>
                            </div>

                            <div class="form-group">
                                <label for="email-1">Mobile Number</label>
                                <input type="text" id="mobile_no" class="form-control" name="mobile_no" required>
                            </div>

                            <div class="form-group">
                                <label for="email-1">Password</label>
                                <input type="text" id="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group">
                                <label for="email-1">Status</label>
                                <select class="form-control" name="status" required>
                                    <option value="1">Active</option>
                                    <option value="0">InActive</option>
                                </select>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary text-uppercase">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div id="officeEditModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Employee Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <form action="{{ route('edit.employee.details') }}" method="post" enctype="multipart/form-data">

                        @csrf
                        <input type="hidden" name="id" id="emp_id">
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="email-1">Name</label>
                                <input type="text" id="editName" class="form-control" name="name" required>
                            </div>

                            <div class="form-group">
                                <label for="email-1">Email</label>
                                <input type="text" id="editEmail" class="form-control" name="email" required>
                            </div>

                            <div class="form-group">
                                <label for="email-1">Mobile Number</label>
                                <input type="text" id="editMobile_no" class="form-control"  disabled>
                            </div>


                            <div class="form-group">
                                <label for="email-1">Status</label>
                                <select type="text" id="editStatus" class="form-control" name="status" required>
                                    <option value="1">Active</option>
                                    <option value="0">InActive</option>
                                </select>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary text-uppercase">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="changepassword" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Change Password</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <form action="{{ route('change.employee.password') }}" method="post" enctype="multipart/form-data">

                        @csrf
                        <input type="hidden" name="id" id="emp_id">
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="email-1">New Password</label>
                                <input type="text" id="newpassword" class="form-control" name="password" required>
                            </div>

                            <div class="form-group">
                                <label for="email-1">Confirm Password</label>
                                <input type="text" id="confirmPassword" class="form-control confirmPassword" name="confirm_password" required>
                                <span id="error-confirm"></span>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary text-uppercase submit-change">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</div>
    <script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
    <script>
        $(".js-edit-logo").on('click',function (e) {
            var id =   $(this).attr('data-id');
            var name =   $(this).attr('data-name');
            var email =   $(this).attr('data-email');
            var mobile =   $(this).attr('data-mobile');
            var status =   $(this).attr('data-status');
            $("#officeEditModal .modal-dialog #emp_id").val(id);
            $("#officeEditModal .modal-dialog #editName").val(name);
            $("#officeEditModal .modal-dialog #editEmail").val(email);
            $("#officeEditModal .modal-dialog #editMobile_no").val(mobile);
            $("#officeEditModal .modal-dialog #editStatus").val(status);
        });
    </script>
    <script>
        $('.js-change-password').on('click',function (e) {
            var id =   $(this).attr('data-id');
            $("#changepassword .modal-dialog #emp_id").val(id);
        })
    </script>

    <script>
        $('.confirmPassword').on('change',function () {
            var password = $('#newpassword').val();
            var confirmPassword = $('#confirmPassword').val();
            if(password != confirmPassword)
            {
                $('#error-confirm').show();
              $('#error-confirm').css('color','red');
              $('#error-confirm').html('Confirm password and password not match.');
              $('.submit-change').attr("disabled", true);

            }else{
                $('.submit-change').attr("disabled", false);
                $('#error-confirm').hide();
            }

        })
    </script>
@endsection
