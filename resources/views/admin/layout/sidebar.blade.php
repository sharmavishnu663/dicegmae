<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
{{--                @if(Auth::user()->profile_image !='')--}}
{{--                    <img src="{{ Auth::user()->profile_image }}" class="img-circle elevation-2" alt="User Image">--}}
{{--                @else--}}
                <img src="{{ asset('admin/assets/img/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
{{--                    @endif--}}
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="{{route('dashboard')}}" class="nav-link {{  Request::path() == 'admin/dashboard'? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
{{--                <li class="nav-item has-treeview">--}}
{{--                    <a href="{{route('admin.employee.list')}}" class="nav-link {{  Request::path() == 'admin-employee-list'? 'active' : '' }}">--}}
{{--                        <i class="nav-icon fas fa-tachometer-alt"></i>--}}
{{--                        <p>--}}
{{--                            Employee List--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li class="nav-item has-treeview">
                    <a href="{{route('admin.user.list')}}" class="nav-link {{  Request::path() == 'admin-users-list'? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            User Data
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('admin.user.list')}}" class="nav-link ">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            User State
                        </p>
                    </a>
                </li>

{{--                <li class="nav-item has-treeview">--}}
{{--                    <a href="{{route('admin.management')}}" class="nav-link {{  Request::path() == 'admin/management'? 'active' : '' }}">--}}
{{--                        <i class="nav-icon fas fa-tachometer-alt"></i>--}}
{{--                        <p>--}}
{{--                            Admin Management--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                </li>--}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
