@extends('admin.layout.app')
<!-- Content Wrapper. Contains page content -->

@section('content')
    <style>
        .save-content{
            margin-top: 8px;
            float: right;
        }
    </style>
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                Admin List
                            </h3>
                            <!-- tools box -->
{{--                            <div class="card-tools">--}}
{{--                                <button  class="btn btn-primary"  data-toggle="modal" data-target="#userAddModal" style="float: right">Add User</button>--}}
{{--                            </div>--}}
                            <!-- /. tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body pad">
                            <div class="tab-content">

                                <!-- Tab panel -->
                                <div id="tab-pane1" class="tab-pane active">

                                    <!-- Card Body -->
                                    <div class="card-body">

                                        <!-- Card Title-->
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
{{--                                                <th>Profile</th>--}}
                                                <th>Employee Name</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Mobile</th>
                                                <th>Address</th>
                                                <th>DOB</th>
                                                <th>Status</th>
                                                <th>Package Details</th>
                                                <th>Package Type</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
{{--                                            @foreach($users as $user)--}}
{{--                                                {{ dd($user) }}--}}
{{--                                                <tr>--}}
{{--                                                    <td> {{ $user->employee->name }}</td>--}}
{{--                                                    <td>{{ $user->name }}</td>--}}
{{--                                                    <td>{{ $user->email}}</td>--}}
{{--                                                    <td>{{ $user->mobile_no}}</td>--}}
{{--                                                    <td>{{ $user->address}}</td>--}}
{{--                                                    <td>{{ $user->info1}}</td>--}}
{{--                                                    <td>@if($user->status =='1') Activate @else Inactive @endif</td>--}}
{{--                                                    <td>{{ $user->info2}}</td>--}}
{{--                                                    <td>{{ $user->info3}}</td>--}}


{{--                                                    <td>--}}
{{--                                                        <a class="delete-material" href="{{ route('delete.user',$user->id) }}"  data-id="{{ $user['id'] }}"  title="delete logo" onClick="return  confirm('Are you sure you want to delete ?')"><i class="fa fa-trash-alt"></i></a--}}
{{--                                                        </td>--}}
{{--                                                </tr>--}}
{{--                                            @endforeach--}}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-->
            </div>

{{--            <div id="userAddModal" class="modal fade" role="dialog">--}}
{{--                <div class="modal-dialog">--}}
{{--                    <!-- Modal content-->--}}
{{--                    <div class="modal-content">--}}
{{--                        <div class="modal-header">--}}
{{--                            <h4 class="modal-title">Add User</h4>--}}
{{--                            <button type="button" class="close" data-dismiss="modal">&times;</button>--}}

{{--                        </div>--}}
{{--                        <form action="{{ route('admin.add.user') }}" method="post" enctype="multipart/form-data">--}}

{{--                            @csrf--}}
{{--                            <input type="hidden" name="id">--}}
{{--                            <div class="modal-body">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="exampleInputEmail1">Profile Image*</label>--}}
{{--                                    <input type="file" class="form-control" name="profile_image" value="{{old('profile_image')}}" required>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="email-1">Name*</label>--}}
{{--                                    <div><input type="text" class="form-control" name="name" value="{{ old('name') }}" required></div>--}}
{{--                                </div>--}}

{{--                                <div class="form-group">--}}
{{--                                    <label for="email-1">Uid*</label>--}}
{{--                                    <div><input type="text" class="form-control" name="uid" value="{{ old('uid') }}" required></div>--}}
{{--                                </div>--}}

{{--                                <div class="form-group">--}}
{{--                                    <label for="email-1">Password*</label>--}}
{{--                                    <div><input type="password" class="form-control" name="password" value="{{ old('password') }}" required></div>--}}
{{--                                </div>--}}

{{--                                <div class="form-group">--}}
{{--                                    <label for="email-1">Email*</label>--}}
{{--                                    <div><input type="text" class="form-control" name="email" value="{{ old('email') }}" required>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="form-group">--}}
{{--                                    <label for="email-1">Role*</label>--}}
{{--                                    <div>--}}
{{--                                        <select name="role_type" required>--}}
{{--                                            <option value="" disabled selected>Select Role Type</option>--}}
{{--                                            <option value="employee" {{old('role_type')=='employee'?'selected':''}}>Employee</option>--}}
{{--                                            <option value="manager" {{old('role_type')=='manager'?'selected':''}}>Manager</option>--}}
{{--                                            <option value="admin" {{old('role_type')=='admin'?'selected':''}}>Admin</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="form-group">--}}
{{--                                    <label for="email-1">Office*</label>--}}
{{--                                    <div>--}}
{{--                                        <select name="office_id[]" required multiple>--}}
{{--                                            <option value="" disabled selected>Select Office Type</option>--}}
{{--                                            @foreach ($offices as $office)--}}
{{--                                                <option value="{{ $office->id }}"--}}
{{--                                                        {{ (collect(old('office_id'))->contains($office->id)) ? 'selected':'' }}--}}
{{--                                                />--}}
{{--                                                {{ $office->name }}--}}
{{--                                                </option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}


{{--                            </div>--}}
{{--                            <div class="modal-footer">--}}
{{--                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
{{--                                <button type="submit" class="btn btn-primary text-uppercase">Submit</button>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div id="userEditModal" class="modal fade" role="dialog">--}}
{{--                <div class="modal-dialog">--}}
{{--                    <!-- Modal content-->--}}
{{--                    <div class="modal-content">--}}
{{--                        <div class="modal-header">--}}
{{--                            <h4 class="modal-title">Edit User Details</h4>--}}
{{--                            <button type="button" class="close" data-dismiss="modal">&times;</button>--}}

{{--                        </div>--}}
{{--                        <form action="{{ route('admin.update.user') }}" method="post" enctype="multipart/form-data">--}}

{{--                            @csrf--}}
{{--                            <input type="hidden" name="id" id="userid">--}}
{{--                            <div class="modal-body">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="exampleInputEmail1">Profile Image</label>--}}
{{--                                    <input type="file" class="form-control" name="profile_image" id="profile">--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="email-1">Name</label>--}}
{{--                                    <div><input type="text" id="name" class="form-control" name="name" required></div>--}}
{{--                                </div>--}}

{{--                                <div class="form-group">--}}
{{--                                    <label for="email-1">Email</label>--}}
{{--                                    <div><input type="text" id="email" class="form-control" name="email" required>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="form-group">--}}
{{--                                    <label for="email-1">Role</label>--}}
{{--                                    <div>--}}
{{--                                        <select name="role_type" id="role">--}}
{{--                                            <option value="" disabled selected>Select Role Type</option>--}}
{{--                                            <option value="employee">Employee</option>--}}
{{--                                            <option value="manager">Manager</option>--}}
{{--                                            <option value="admin">Admin</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="form-group">--}}
{{--                                    <label for="email-1">Office</label>--}}
{{--                                    <div>--}}
{{--                                        <select name="office_id[]" id="office" multiple>--}}
{{--                                            @foreach ($offices as $office)--}}
{{--                                                <option value="{{ $office->id }}">{{ $office->name }}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}


{{--                            </div>--}}
{{--                            <div class="modal-footer">--}}
{{--                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
{{--                                <button type="submit" class="btn btn-primary text-uppercase">Submit</button>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
    <script>
        // $(".js-edit-logo").on('click',function (e) {
        //     var id =   $(this).attr('data-id');
        //     var name =   $(this).attr('data-name');
        //     var email =   $(this).attr('data-email');
        //     var role =   $(this).attr('data-role');
        //     var office =   $(this).attr('data-office');
        //     $("#userEditModal .modal-dialog #userid").val(id);
        //     $("#userEditModal .modal-dialog #name").val(name);
        //     $("#userEditModal .modal-dialog #email").val(email);
        //     $("#userEditModal .modal-dialog #role option:selected").removeAttr("selected");
        //     var roleid = '#userEditModal .modal-dialog #role option[value=' + role +']';
        //     $(roleid).attr('selected', 'selected');
        //     $("#userEditModal .modal-dialog #office option:selected").removeAttr("selected");
        //     JSON.parse(office).forEach(function (offic) {
        //         var officeid = '#userEditModal .modal-dialog #office option[value=' + offic.id +']';
        //         $(officeid).attr('selected', 'selected');
        //     });
        // });
    </script>
@endsection
